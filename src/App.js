
import React from "react";
import Navbar from "./section/navbar/navbar";
import Intro from "./section/intro/intro";
import AboutUs from "./section/AboutUs/AboutUs";
import ComingSoon from "./section/ComingSoon/ComingSoon";
import RoadMap from "./section/RoadMap/RoadMap";
import GeneralRoad from "./section/GeneralRoad/GeneralRoad";
import Footer from "./section/Footer/Footer";

//s
function App() {
  return (
    <>
    <div class="cir">
        <img src="https://circless.s3.sa-east-1.amazonaws.com/GreyCircle2.338315e8.png" class="circle2" alt="" />
    </div>
    <div className="cir">
       <img src="https://circless.s3.sa-east-1.amazonaws.com/GreyCircle2.338315e8.png" class="circle" alt="" />
    </div>
    <div className="cir">
       <img src="https://circless.s3.sa-east-1.amazonaws.com/GreyCircle2.338315e8.png" class="circle3" alt="" />
    </div>
    <div className="cir">
       <img src="https://circless.s3.sa-east-1.amazonaws.com/GreyCircle2.338315e8.png" class="circle4" alt="" />
    </div>
    <div className="cir">
       <img src="https://circless.s3.sa-east-1.amazonaws.com/GreyCircle2.338315e8.png" class="circle5" alt="" />
    </div>
    <div className="cir">
       <img src="https://circless.s3.sa-east-1.amazonaws.com/GreyCircle2.338315e8.png" class="circle6" alt="" />
    </div>
    
      <Navbar />
      <Intro />
      <AboutUs />
      <ComingSoon />
      <RoadMap />
      <GeneralRoad />
      <Footer />
    </>
  );
}

export default App;
